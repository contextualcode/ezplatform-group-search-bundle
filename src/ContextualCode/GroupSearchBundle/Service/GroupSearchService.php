<?php

namespace ContextualCode\GroupSearchBundle\Service;

use eZ\Publish\API\Repository\Exceptions\InvalidArgumentException;
use eZ\Publish\API\Repository\SearchService;
use eZ\Publish\API\Repository\Values\Content\Content;
use eZ\Publish\API\Repository\Values\Content\ContentInfo;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\Core\MVC\ConfigResolverInterface;
use EzSystems\EzPlatformRichText\eZ\RichText\Converter;
use EzSystems\EzPlatformSolrSearchEngine\Gateway\Native;
use Symfony\Component\Routing\RouterInterface;

class GroupSearchService
{
    protected $groupsConfig;
    /** @var SearchService */
    protected $searchService;
    /** @var RouterInterface */
    private $router;
    /** @var Native */
    private $solrGateway;
    /** @var ConfigResolverInterface */
    private $configResolver;
    /** @var Converter */
    private $richtextConverter;

    public function __construct(
        SearchService $searchService,
        RouterInterface $router,
        Native $solrGateway,
        ConfigResolverInterface $configResolver,
        Converter $richtextConverter
    ) {
        $this->searchService = $searchService;
        $this->router = $router;
        $this->solrGateway = $solrGateway;
        $this->configResolver = $configResolver;
        $this->richtextConverter = $richtextConverter;
    }

    /**
     * @param string|int $groupIdentifier
     * @param string|int $term
     * @return array
     */
    public function findGroupItems($groupIdentifier, $term): array
    {
        $config = $this->getGroupsConfig()[$groupIdentifier];

        $query = new Query();
        $query->limit = $config['max_count'] ?? 25;
        $term = $this->parseInputTerm((string)$term);
        if ($term) {
            $query->query = new Criterion\FullText($term);
        }

        $criteria = [new Criterion\Visibility(Criterion\Visibility::VISIBLE)];
        if (!empty($config['include_content_types'])) {
            $criteria[] = new Criterion\ContentTypeIdentifier($config['include_content_types']);
        } elseif (!empty($config['exclude_content_types'])) {
            $criteria[] = new Criterion\LogicalNot(
                new Criterion\ContentTypeIdentifier($config['exclude_content_types'])
            );
        }
        if (!empty($config['parent_location_path'])) {
            $criteria[] = new Criterion\Subtree($config['parent_location_path']);
        }
        $query->filter = $criteria ? new Criterion\LogicalAnd($criteria) : null;

        $this->solrGateway->setHighlights((bool)$term);
        try {
            $hits = $this->searchService->findContentInfo($query)->searchHits;
        } catch (InvalidArgumentException $e) {
            $hits = [];
        }
        $this->solrGateway->setHighlights(false);

        $items = [];
        foreach ($hits as $hit) {
            /** @var ContentInfo $info */
            $info = $hit->valueObject;
            $items[] = [
                'href' => $this->router->generate('ez_urlalias', ['locationId' => $info->mainLocationId]),
                'title' => $info->name,
                'description' => $hit->highlight ?? '',
            ];
        }

        return $items;
    }

    /**
     * @param string $term
     * @return array[]
     */
    public function findReplyBySearchPattern(string $term): array
    {
        $query = new Query();
        $query->limit = 10000;
        $query->filter = new Criterion\LogicalAnd([
            new Criterion\Visibility(Criterion\Visibility::VISIBLE),
            new Criterion\ContentTypeIdentifier('search_reply'),
        ]);

        try {
            $hits = $this->searchService->findContent($query)->searchHits;
        } catch (InvalidArgumentException $e) {
            $hits = [];
        }

        $items = [];
        foreach ($hits as $hit) {
            /** @var Content $content */
            $content = $hit->valueObject;
            if (preg_match($content->getFieldValue('pattern')->text, $term)) {
                $items[] = [
                    'title' => $content->getFieldValue('title')->text,
                    'description' => $this->richtextConverter->convert(
                        $content->getFieldValue('description')->xml
                    )->saveHTML(),
                ];
            }
        }

        return $items;
    }

    public function getGroupsConfig()
    {
        if (!$this->groupsConfig) {
            $this->groupsConfig = $this->configResolver->getParameter(
                'contextualcode.group_search.groups_definition'
            );
        }

        return $this->groupsConfig;
    }

    protected function parseInputTerm(string $term): string
    {
        $term = trim($term);
        if (!$term) {
            return '';
        }

        return rtrim($term, '*') . '*';
    }
}
