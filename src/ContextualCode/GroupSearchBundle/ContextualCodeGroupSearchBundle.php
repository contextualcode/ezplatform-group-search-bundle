<?php

namespace ContextualCode\GroupSearchBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContextualCodeGroupSearchBundle extends Bundle
{
    protected $name = 'ContextualCodeGroupSearchBundle';
}
