<?php

namespace ContextualCode\GroupSearchBundle\eZ\Solr;

use eZ\Publish\API\Repository\Values\Content\Search\SearchHit;
use eZ\Publish\SPI\Persistence\Content\ContentInfo;
use eZ\Publish\SPI\Persistence\Content\Location;
use EzSystems\EzPlatformSolrSearchEngine\ResultExtractor\NativeResultExtractor as BaseNativeResultExtractor;

/**
 * Override eZ native result extract to extend functionality...
 * - Adding highlighting to results
 */
class NativeResultExtractor extends BaseNativeResultExtractor
{
    /**
     * {@inheritdoc}
     */
    public function extract($data, array $facetBuilders = [])
    {
        $searchResult = parent::extract($data, $facetBuilders);
        if (!$searchResult->searchHits || empty($data->highlighting)) {
            return $searchResult;
        }

        foreach ($data->response->docs as $doc) {
            $hit = $this->getHitByContentId($searchResult->searchHits, $doc->content_id_id ?? null);
            if ($hit) {
                $hit->highlight = $this->getHighlightingText($data->highlighting->{$doc->id});
            }
        }

        return $searchResult;
    }

    /**
     * @param \stdClass $highlight
     * @return string|null
     */
    protected function getHighlightingText(\stdClass $highlight): string
    {
        if (!empty($highlight->meta_content__text_t[0])) {
            $texts = $highlight->meta_content__text_t; // all fields combined already
        } else {
            $texts = [[]];
            foreach ($highlight as $k => $hText) {
                $texts[] = $hText;
            }
            $texts = array_unique(array_merge(...$texts));
        }

        return strip_tags(implode(' ... ', $texts), '<b>');
    }

    /**
     * @param SearchHit[] $searchHits
     * @param int|null $id
     * @return SearchHit|null
     */
    protected function getHitByContentId(array $searchHits, $id)
    {
        if (!$id) {
            return null;
        }
        foreach ($searchHits as $hit) {
            if (
                ($hit->valueObject instanceof ContentInfo && $hit->valueObject->id == $id)
                || ($hit->valueObject instanceof Location && $hit->valueObject->contentId == $id)
            ) {
                return $hit;
            }
        }

        return null;
    }
}
