<?php

namespace ContextualCode\GroupSearchBundle\eZ\Solr\Gateway;

use EzSystems\EzPlatformSolrSearchEngine\Gateway\Native as Base;

class Native extends Base
{
    /** @var bool send hl=on when true. */
    private $highlights = false;

    /**
     * @param bool $highlights
     */
    public function setHighlights(bool $highlights): void
    {
        $this->highlights = $highlights;
    }

    /**
     * {@inheritDoc}
     */
    protected function search(array $parameters)
    {
        // enable highlights for all fields, when FullText search performed
        if ($this->highlights && isset($parameters['q']) && strpos($parameters['q'], ' uf=-*}')) {
            $parameters['hl'] = 'on';
            $parameters['hl.fl'] = '*_text_t';
            $parameters['hl.simple.pre'] = '<b>';
            $parameters['hl.simple.post'] = '</b>';
        }

        return parent::search($parameters);
    }
}
