if (!String.prototype.trim) {
    (function () {
        String.prototype.trim = function () {
            return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
        };
    })();
}

(function () {
    let resultTemplate, groupColumns = [], inputEl, apiURL, searchTerm, lastSearchTerm, searchId,
        patternReplyContainer, patternReplyResultTemplate, pendingReq, isPending = false;

    const hideGroupBlocks = () => {
        for (let i = 0; i < groupColumns.length; i++) {
            groupColumns[i].columnElement.style.display = 'none';
        }
    };
    const showGroupBlocks = () => {
        for (let i = 0; i < groupColumns.length; i++) {
            groupColumns[i].columnElement.style.display = '';
        }
    };

    const showNoResults = columnIdentifier => {
        for (let i = 0; i < groupColumns.length; i++) {
            if (!columnIdentifier || columnIdentifier === groupColumns[i].identifier) {
                groupColumns[i].containerElement.innerHTML = groupColumns[i].noResultsTemplate
                    .replace('{term}', searchTerm);
            }
        }
    };

    const renderGroupResults = (columnIdentifier, resultsData) => {
        let itemsHtml = '';
        for (let i = 0; i < resultsData.length; i++) {
            itemsHtml += getGroupResultItemHtml(resultsData[i]);
        }

        for (let i = 0; i < groupColumns.length; i++) {
            if (groupColumns[i].identifier === columnIdentifier) {
                groupColumns[i].containerElement.innerHTML = itemsHtml;
                return;
            }
        }
    };

    const renderPatternResults = (resultsData) => {
        let itemsHtml = '';
        for (let i = 0; i < resultsData.length; i++) {
            itemsHtml += getPatternResultItemHtml(resultsData[i]);
        }
        patternReplyContainer.innerHTML = itemsHtml;
    };

    const getItemHtml = (itemData, template) => {
        const htmlObject = document.createElement('div');
        htmlObject.innerHTML = template;

        const linkEl = htmlObject.querySelector('[data-gs-item-link]');
        if (linkEl) {
            linkEl.href = itemData.href;
            linkEl.removeAttribute('data-gs-item-link');
        }

        const titleEl = htmlObject.querySelector('[data-gs-item-title]');
        if (itemData.title) {
            titleEl.innerHTML = itemData.title;
            titleEl.removeAttribute('data-gs-item-title');
        } else {
            titleEl.parentNode.removeChild(titleEl);
        }

        const descriptionEl = htmlObject.querySelector('[data-gs-item-description]');
        if (itemData.description) {
            descriptionEl.innerHTML = itemData.description;
            descriptionEl.removeAttribute('data-gs-item-description');
        } else {
            descriptionEl.parentNode.removeChild(descriptionEl);
        }

        return htmlObject.innerHTML;
    };

    const getGroupResultItemHtml = itemData => {
        return getItemHtml(itemData, resultTemplate);
    };

    const getPatternResultItemHtml = itemData => {
        return getItemHtml(itemData, patternReplyResultTemplate);
    };

    const requestResults = () => {
        searchTerm = (inputEl.value || '').toLowerCase().trim();
        if (!searchTerm || searchTerm === lastSearchTerm) {
            return;
        }
        lastSearchTerm = searchTerm;

        if (searchId) {
            window.clearTimeout(searchId);
            searchId = null;
        }
        searchId = window.setTimeout(() => {
            if (isPending && pendingReq) {
                pendingReq.abort();
            }
            isPending = true;

            pendingReq = new XMLHttpRequest();
            const url = apiURL
                + '?term=' + encodeURIComponent(searchTerm)
                + (patternReplyContainer ? '&includePatternReply=Y' : '');
            pendingReq.open('GET', url, true);
            pendingReq.onerror = () => {
                isPending = false;
                if (patternReplyContainer) {
                    showGroupBlocks();
                    patternReplyContainer.style.display = 'none';
                }
                showNoResults();
            };
            pendingReq.onreadystatechange = () => {
                if (pendingReq.readyState !== 4 || pendingReq.status !== 200) {
                    return;
                }

                const response = JSON.parse(pendingReq.responseText);
                const patternResults = response.patternResults;
                const groupedResults = response.groupedResults;
                if (groupedResults) {
                    if (patternReplyContainer) {
                        showGroupBlocks();
                        patternReplyContainer.style.display = 'none';
                    }
                    for (let i = 0; i < groupedResults.length; i++) {
                        const groupedResult = groupedResults[i];
                        if (groupedResult.items && groupedResult.items.length) {
                            renderGroupResults(groupedResult.identifier, groupedResult.items);
                        } else {
                            showNoResults(groupedResult.identifier);
                        }
                    }
                } else if (patternReplyContainer && patternResults) {
                    hideGroupBlocks();
                    patternReplyContainer.style.display = '';
                    renderPatternResults(patternResults);
                } else {
                    showGroupBlocks();
                    showNoResults();
                }
                isPending = false;
            };
            pendingReq.send();
        }, 100);
    };

    const attachEvents = () => {
        inputEl.addEventListener('keyup', requestResults);
    };

    const initialize = () => {
        inputEl = document.getElementById('gs-search-input');
        if (!inputEl) {
            return;
        }
        resultTemplate = inputEl.dataset.resultTemplate;

        const columns = document.querySelectorAll('[data-group-result-identifier]');
        for (let i = 0; i < columns.length; i++) {
            const containerElement = columns[i].querySelector('[data-group-results-container]');
            groupColumns.push({
                'identifier': columns[i].dataset.groupResultIdentifier,
                'noResultsTemplate': columns[i].dataset.noResultsTemplate,
                'containerElement': containerElement,
                'columnElement': columns[i],
            });
        }
        apiURL = inputEl.dataset.requestUri;

        patternReplyContainer = document.getElementById('gs-pattern-reply');
        if (patternReplyContainer) {
            patternReplyResultTemplate = patternReplyContainer.dataset.resultTemplate;
        }

        attachEvents();
    };

    initialize();
})();
